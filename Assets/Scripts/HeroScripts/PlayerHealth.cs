using System.Collections;
using System.Collections.Generic;
using UnityEditor.SearchService;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class PlayerHealth : MonoBehaviour
{
    // Start is called before the first frame update
    public float maxHealth;
    public float minHealth;
    float currentHealth;
    [SerializeField] Animator animator;
    public Slider playerHealthSlider;
    void Start()
    {
        currentHealth = maxHealth;
        playerHealthSlider.maxValue = maxHealth;
        playerHealthSlider.value=maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void addDamage(float damage)
    {
        if (currentHealth == 0) {
            animator.SetBool("noBlood", true);
            animator.SetTrigger("Death");

        }
        else
        {
            currentHealth -= damage;
            playerHealthSlider.value = currentHealth;
            animator.SetTrigger("Hurt");
        }
     
    }
}
