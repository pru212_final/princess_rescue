using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float damage;
    float dameRate = 0.5f;
    public float pushBackForce;
    float nextDamage;
    private HeroKnight heroKnight;
    // Start is called before the first frame update
    void Start()
    {
        nextDamage = 0;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
      

    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player" && nextDamage < Time.time)
        {
            PlayerHealth playerHealth = other.gameObject.GetComponent<PlayerHealth>();
            heroKnight= other.gameObject.GetComponent<HeroKnight>();
            if(!heroKnight.block) {
                playerHealth.addDamage(damage);
            }
            else
            {
                Debug.Log("No dame");
            }
            nextDamage = dameRate + Time.time;
            pushBack(other.transform);
        }
    }
    void pushBack(Transform pushedObject)
    {
        Vector2 pushDrirection = new Vector2(0, (pushedObject.position.y - transform.position.y)).normalized;
        pushDrirection *= pushBackForce;
        Rigidbody2D pushRB = pushedObject.gameObject.GetComponent<Rigidbody2D>();
        pushRB.velocity = Vector2.zero;
        pushRB.AddForce(pushDrirection,ForceMode2D.Impulse);
    }
}
